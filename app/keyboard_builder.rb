require 'telegram/bot'

module KeyboardBuilder
  def generate_menu_keyboard
    buttons = ['Меню', 'Корзина'].map { |name| create_keyboard_button(name) }
    Telegram::Bot::Types::ReplyKeyboardMarkup.new(keyboard: buttons)
  end

  def generate_contact_keyboard
    Telegram::Bot::Types::ReplyKeyboardMarkup.new(
        keyboard: create_keyboard_button('Отправить мой телефон', request_contact: true)
    )
  end

  def generate_location_keyboard
    Telegram::Bot::Types::ReplyKeyboardMarkup.new(
        keyboard: create_keyboard_button('Share my location', request_location: true)
    )
  end

  def create_keyboard_button(text, request_contact: false, request_location: false)
    Telegram::Bot::Types::KeyboardButton.new(
        text: text,
        request_contact: request_contact,
        request_location: request_location
    )
  end
end
