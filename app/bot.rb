$LOAD_PATH << '.'

require 'telegram/bot'
require 'pry'
require 'pry-byebug'
require 'colorize'
require 'geocoder'
require 'keyboard_builder'
require 'inline_keyboard_builder'
require 'cart'

class Bot
  include KeyboardBuilder
  include InlineKeyboardBuilder

  TOKEN = '909281980:AAHPb2suL66sKrSA817ZrH0KEiMe6jdYg5k'

  def run
    Telegram::Bot::Client.run(TOKEN) do |bot|
      @cart = Cart.new
      @logger = Logger.new(STDOUT)
      @pizzas_data = JSON(File.read('pizza_data.json'))
      bot.listen do |message|
        @user_name = [message.from.first_name, message.from.last_name].compact.join(' ')
        reply(bot, message)
      end
    end
  end

  def reply(bot, message)
    case message
    when Telegram::Bot::Types::CallbackQuery
      reply_for_callback(bot, message)
    when Telegram::Bot::Types::Message
      reply_for_message(bot, message)
    end
  end

  def reply_for_message(bot, message)
    case message.text
    when 'Меню'
      output_pizza_info(bot, message)
    when 'Корзина'
      bot.api.send_message(chat_id: message.chat.id, text: @cart.show_cart, reply_markup: @cart.any? ? generate_cart_buttons : nil)
    when '/start'
      bot.api.send_message(chat_id: message.chat.id, text: "Привет, #{message.from.first_name}", reply_markup: generate_menu_keyboard)
    when '/help'
      bot.api.send_message(chat_id: message.chat.id, text: 'Its my pizza bot, enjoy!')
    else
      return @cart.add_phone_number(bot, message) unless message.contact.nil?
      return @cart.add_address(bot, message) if address_message?(message.reply_to_message&.text)
      bot.api.send_message(chat_id: message.chat.id, text: 'Проверьте правильность введенной команды!')
    end
  end

  def address_message?(text)
    text == "Введите адрес доставки ответом на это сообщение в формате:\nНАЗВАНИЕ УЛИЦЫ, НОМЕР ДОМА, КВАРТИРА, ЭТАЖ"
  end

  def output_pizza_info(bot, message)
    @logger.info("Start output pizzas for #{@user_name}".yellow)
    @pizzas_data.map do |pizza|
      Thread.new {
        keyboard = generate_inline_keyboard('Виды', pizza['id'])
        bot.api.send_photo(chat_id: message.chat.id, photo: pizza['image'], caption: pizza['name'], reply_markup: keyboard)
      }
    end.each(&:join)
    @logger.info("All pizzas for #{@user_name} sended")
  end

  def reply_for_callback(bot, message)
    case
    when pizza_selected?(message.data)
      pizza = @pizzas_data.find { |pizza| pizza['id'] == message.data }
      bot.api.edit_message_reply_markup(chat_id: message.message.chat.id, message_id: message.message.message_id, reply_markup: generate_variation_keyboard(pizza['variations']))
    when add_to_cart?(message.data)
      @cart.add_to_cart(message.message.caption, message.data.split('_').last, clicked_button_text(message))
    when office_order?(message.data)
      @cart.make_office_order(bot, message)
    when make_order?(message.data)
      @cart.collect_user_info(bot, message, :address_collection)
    when street_name_info?(message.data)
      @cart.add_street_name(bot, message)
    when select_payment_method?(message.data)
      @cart.set_payment_method(message.data)
      @cart.collect_user_info(bot, message, :phone_collection)
    when confirm_order?(message.data)
      @cart.confirm_order(bot, message)
      @logger.info("User #{@user_name} confirmed order for total price: #{@cart.calculate_sum_price}".green)
    when edit_order_info?(message.data)
      @cart.clear_order_info
      @cart.make_office_order(bot, message)
    when cancel_order?(message.data)
      @cart.cancel_order
    when clear_cart?(message.data)
      @cart.clear_cart
      bot.api.send_message(chat_id: message.message.chat.id, text: 'Ваша карзина пуста!')
    else
      # type code here
    end
  end

  def pizza_selected?(data)
    @pizzas_data.map { |x| x['id'] }.include?(data)
  end

  def add_to_cart?(data)
    data.split('_').first == 'add'
  end

  def clicked_button_text(message)
    message.message.reply_markup.inline_keyboard.flatten.find { |x| x['callback_data'] == message.data }['text']
  end

  def office_order?(data)
    data == 'office_order'
  end

  def make_order?(data)
    data == 'make_order'
  end

  def clear_cart?(data)
    data == 'clear_cart'
  end

  def confirm_order?(data)
    data == 'confirm_order'
  end

  def cancel_order?(data)
    data == 'cancel_order'
  end

  def edit_order_info?(data)
    data == 'edit_order_info'
  end

  def street_name_info?(data)
    data.include?('street_name')
  end

  def select_payment_method?(data)
    data.include?('payment=')
  end
end
