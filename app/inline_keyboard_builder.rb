require 'telegram/bot'

module InlineKeyboardBuilder
  def generate_inline_keyboard(text, callback_data)
    Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: keyboard_button(text, callback_data))
  end

  def generate_variation_keyboard(variations)
    keyboard = variations.map do |variation|
      keyboard_button(
          [variation['variation_name'], '  --  ', variation['price']].join(' '),
          ['add', variation['id']].join('_')
      )
    end
    Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: keyboard)
  end

  def generate_cart_buttons
    Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: [
        keyboard_button('Заказать в офис', 'office_order'),
        keyboard_button('Очистить корзину', 'clear_cart'),
        keyboard_button('Оформить заказ на свой адрес', 'make_order'),
    ])
  end

  def generate_confirm_keyboard
    Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: [
        keyboard_button('Да, все верно!', 'confirm_order'),
        keyboard_button('Изменить данные', 'edit_order_info'),
        keyboard_button('Отменить заказ', 'cancel_order'),
    ])
  end

  def generate_streets_keyboard(streets)
    Telegram::Bot::Types::InlineKeyboardMarkup.new(
        inline_keyboard: streets.map { |street| keyboard_button(street, 'street_name') }
    )
  end

  def payment_method_buttons(payment_methods)
    keyboard = payment_methods.map do |key, name|
      keyboard_button(name, "payment=#{key}")
    end
    Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: keyboard)
  end

  def keyboard_button(text, callback_data)
    Telegram::Bot::Types::InlineKeyboardButton.new(text: text, callback_data: callback_data)
  end
end
