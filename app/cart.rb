require './order'
require './keyboard_builder.rb'
require './inline_keyboard_builder.rb'

class Cart
  include Order
  include KeyboardBuilder
  include InlineKeyboardBuilder

  def initialize
    @products = {}
    @user_info = {}
  end

  def add_to_cart(pizza_name, pizza_id, info)
    @products.keys.include?(pizza_id) ? increase_count(pizza_id) : add_pizza_info(pizza_name, pizza_id, info)
  end

  def increase_count(pizza_id)
    @products[pizza_id][:count] += 1
  end

  def add_pizza_info(pizza_name, pizza_id, info)
    @products[pizza_id] = {
        count: 1,
        name: [pizza_name, info.split('  --  ').first].join(' '),
        price: info.split('  --  ').last.to_f
    }
  end

  def show_cart
    @products.empty? ? 'Ваша корзина пуста' : "Ваш заказ:\n
#{@products.values.map { |x| "#{x[:name]}   x#{x[:count]}  =  #{(x[:count] * x[:price]).round(2)}" }.join(" руб.\n")} руб.
----------\nИтого:  #{calculate_sum_price} руб."
  end

  def calculate_sum_price
    @products.values.map { |x| x[:count] * x[:price] }.reduce(:+).round(2)
  end

  def clear_cart
    @products = {}
  end

  def any?
    @products.any?
  end
end
