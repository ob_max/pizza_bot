require 'curb'

module Order
  ORDER_URL = 'https://backend.dominos.by/api/order/delivery/?city_id=2&lang=ru'
  PAYMENT_METHOD = {cash: 'Наличными', cash_card_delivery: 'Картой курьеру'}
  ADDRESS_KEYS = [:street_name, :street_house, :suite, :floor]

  def make_office_order(bot, message)
    @user_info[:street_name] = 'ЛЬВА ТОЛСТОГО УЛ.'
    @user_info[:street_house] = 10
    collect_user_info(bot, message, :payment)
  end

  def add_address(bot, message)
    user_data = message.text.split(',').map(&:strip)
    address_info = Hash[ADDRESS_KEYS.map.with_index { |key, index| [key, user_data[index]]}]
    @user_info[:street_house] = address_info[:street_house]
    @user_info[:suite] = address_info[:suite]
    @user_info[:floor] = address_info[:floor]
    parse_street_name(address_info[:street_name], bot, message.chat.id)
  end

  def parse_street_name(name, bot, chat_id)
    streets = JSON(File.read('street_data.json')).select { |street| street.downcase.include?(name.downcase) }
    bot.api.send_message(chat_id: chat_id, text: 'Выберите вашу улицу:', reply_markup: generate_streets_keyboard(streets))
  end

  def add_street_name(bot, message)
    @user_info[:street_name] = message.message.reply_markup.inline_keyboard.flatten.find { |x| x['callback_data'] == message.data }['text']
    collect_user_info(bot, message, :payment)
  end

  def set_payment_method(data)
    @user_info[:payment] = data.split('=').last
  end

  def add_phone_number(bot, message)
    @user_info[:phone] = message.contact.phone_number[/375(.*)/, 1]

    bot.api.send_message(chat_id: message.chat.id, text: 'Еще чуть-чуть ...', reply_markup: generate_menu_keyboard)
    bot.api.send_message(chat_id: message.chat.id, text: "Проверьте правильность заказа\n#{full_order_output}", reply_markup: generate_confirm_keyboard)
  end

  def full_order_output
    [
        show_cart,
        "Тип оплаты - #{PAYMENT_METHOD[@user_info[:payment].to_sym]}",
        "Доставить по адресу: #{@user_info[:street_name]} #{@user_info[:street_house]}",
        "Курьер позвонит по намеру: +375 #{@user_info[:phone]}"
    ].join("\n")
  end

  def confirm_order(bot, message)
    response_body = JSON(send_delivery_request)
    if response_body['status'] == 0
      bot.api.send_message(chat_id: message.message.chat.id, text: "Ваш заказ успешно оформлен, ожидайте звонка =)")
    else
      bot.api.send_message(chat_id: message.message.chat.id, text: "Что-то не так, проверьте введенные данные")
    end
  end

  def collect_user_info(bot, message, step)
    case step
    when :payment
      bot.api.send_message(chat_id: message.message.chat.id, text: 'Выбарите тип оплаты:', reply_markup: payment_method_buttons(PAYMENT_METHOD))
    when :phone_collection
      bot.api.send_message(chat_id: message.message.chat.id, text: 'Поделитесь своим номером для оформления заказа!', reply_markup: generate_contact_keyboard)
    when :address_collection
      bot.api.send_message(chat_id: message.message.chat.id, text: "Введите адрес доставки ответом на это сообщение в формате:\nНАЗВАНИЕ УЛИЦЫ, НОМЕР ДОМА, КВАРТИРА, ЭТАЖ")
    end
  end

  def send_delivery_request
    curl = Curl::Easy.new(ORDER_URL)
    curl.post_body = URI.encode(build_body_params.map { |k,v| "#{k}=#{v}" }.join('&'))
    curl.perform
    curl.body_str
  end

  def build_body_params
    {
        cart: JSON(
          products: Hash[@products.map { |k, v| [k, v[:count]] }],
          products_custom: [],
          total: calculate_sum_price
        ),
        street_house: @user_info[:street_house],
        coupon: nil,
        street_name: @user_info[:street_name],
        floor: @user_info[:floor],
        order_source: 'mobile_site',
        phone: @user_info[:phone],
        date: (Time.now + 30 * 60).strftime("%Y-%m-%d %H:%M"),
        entrance: nil,
        pay_method: @user_info[:payment],
        suite: @user_info[:suite],
        api_key: 'B3pl8vGDjMdh',
        email: 'guest@dominos.by',
        door_code: nil
    }
  end

  def cancel_order
    clear_cart
    clear_order_info
  end

  def clear_order_info
    @user_info = {}
  end
end
