require 'curb'
require 'pry'
require 'json'

class Parser
  SEED_PIZZA_URL = 'https://backend.dominos.by/api/products/?api_key=B3pl8vGDjMdh&city_id=2&lang=ru'
  STREETS_URL = 'https://backend.dominos.by/api/streets/?api_key=B3pl8vGDjMdh&city_id=2&lang=ru'
  START_IMAGE_URL = 'https://images.dominos.by/media'
  PIZZA_TYPES = {
      'HD' => 'Хот-Дог борт',
      'ULT' => 'Ультратонкое',
      'ITAL' => 'Тонкое',
      'CREM' => 'Сырный борт',
      '' => 'Классика',
  }

  def run
    [
        Thread.new { save_data(pizza_info, type: 'pizza') },
        Thread.new { save_data(streets_info, type: 'street') }
    ].each(&:join)
  end

  def pizza_info
    pizzas = get_pizzas_from_seed_url
    pizzas.map do |pizza|
      {
          id: pizza['id'].to_s,
          name: pizza['name'],
          image: URI.join(START_IMAGE_URL, pizza['photo_small']).to_s,
          variations: parse_pizza_variations(pizza['products'])
      }
    end.flatten
  end

  def streets_info
    curl = Curl::Easy.perform(STREETS_URL)
    JSON(curl.body_str).dig('data').keys
  end

  def get_pizzas_from_seed_url
    curl = Curl::Easy.perform(SEED_PIZZA_URL)
    json = JSON(curl.body_str)
    json.dig('data').values.select { |x| x['product_category'] == 'Pizza' }
  end

  def parse_pizza_variations(variations)
    variations.map do |id, variation_data|
      next if variation_data['size'].include?('PAN')

      variation_name = variation_data['size'].match(/([A-Z]*)\s*(\d+)/)[1, 2]
      {
      id: id,
      variation_name: [PIZZA_TYPES[variation_name.first], variation_name[1], 'cm'].join(' '),
      price: variation_data['price']
      }
    end.compact
  end

  def save_data(content, type: )
    File.open("#{type}_data.json", 'w') { |f| f.write(JSON(content)) }
  end
end

Parser.new.run